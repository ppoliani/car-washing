export default {
    cryptos: {
        algorithm: "aes256",
        key: process.env.CRYPTO_KEY || "Your crypto key goes here"
    }
};
