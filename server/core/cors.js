export default function (app) {
    app.use(function *(next) {
        this.set('Access-Control-Allow-Origin', '*');
        this.set('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        this.set('Access-Control-Allow-Headers', 'Content-Type, Authorization');

        // intercept OPTIONS method
        if ('OPTIONS' == this.request.method) {
            this.send(200);
        }
        else {
            yield next;
        }
    });
}
