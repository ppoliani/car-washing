import { Injector, bind } from "jecht";
import logger from "../utils/logger";
import requestBodyParser from "../utils/requestBodyParser";
import credentialParser from "../utils/credentialParser";

var injector = new Injector([
    bind("logger").toValue(logger),
    bind("requestBodyParser").toValue(requestBodyParser),
    bind("credentialParser").toValue(credentialParser)
]);

/**
 *
 * @param {Class} dep
 * @returns {Any}
 */
export function resolve(dep) {
    return injector.get(dep);
}
