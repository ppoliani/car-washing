import auth from "../auth";

const modules = [
    auth
];

export default function(appRouter) {
    modules.forEach((module) =>  {
        module.routes(appRouter)
    });

    return appRouter;
}
