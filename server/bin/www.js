#!/usr/bin/env node
import fs from "fs";
import http from "http";
import https from "https";
import path from "path";
import debugModule from "debug";
import bootstrap from "../app";
import dotenv from "dotenv";

const debug = debugModule("dev");

dotenv.load();

const app = bootstrap();

const sslOptions = {
    key  : fs.readFileSync(path.join(__dirname, "../certificates/server.key")),
    cert : fs.readFileSync(path.join(__dirname, "../certificates/server.crt"))
};

//const server = http.createServer(sslOptions, app.callback()).listen(process.env.SERVER_PORT, () => {
//    debug("Koa server listening on port " + server.address().port);
// });

const server = http.createServer(app.callback()).listen(process.env.SERVER_PORT, () => {
    debug("Koa server listening on port " + server.address().port);
});
