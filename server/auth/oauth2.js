import oauth2orize from "oauth2orize";
import passport from "koa-passport";
import co from "co";
import crypto from "crypto";
import jwt from "jwt-simple";
import User from "./models/User";
import AccessToken from "./models/AccessToken";
import RefreshToken from "./models/RefreshToken";

const oauth2Server = oauth2orize.createServer();

oauth2Server.exchange(oauth2orize.exchange.password((client, username, password, scope, done) => {
    co(function* () {
        try {
            const user =  yield User.findOne({ username: username });

            if (!user) {
                return done(null, false);
            }

            if(yield !user.comparePassword(password) ) {
                return done(null, false);
            }

            yield RefreshToken.remove({ userId: user.id, clientId: client.clientId });
            yield AccessToken.remove({ userId: user.id, clientId: client.clientId });

            const tokenValue =  {
                expires_in: config.get('security:tokenLife'),
                userName: user.username,
                claims: user.claims
            };

            const jwtToken = jwt.encode(tokenValue, config.get('security:jwt_secret'));
            const refreshTokenValue = crypto.randomBytes(32).toString('base64');
            const token = new AccessToken({
                token: jwtToken,
                clientId: client.clientId,
                userId: user.id,
                created: new Date()
            });
            const refreshToken = new RefreshToken({
                token: refreshTokenValue,
                clientId: client.clientId,
                userId: user.id
            });

            yield refreshToken.save();
            yield token.save();

            return done(null, jwtToken, refreshTokenValue, { user_info: { userName: user.username, id: user.id, claims: user.claims }});
        }
        catch(ex) {
            return done(err);
        }
    });
}));

/**
 * Exchange refreshToken for access token.
 */
oauth2Server.exchange(oauth2orize.exchange.refreshToken(function(client, refreshToken, scope, done) {
    co(function* () {
        try {
            const token = yield RefreshToken.findOne({ token: refreshToken });

            if (!token) {
                return done(null, false);
            }

            const user = yield User.findById(token.userId);

            if(!user) {
                return done(null, false);
            }

            yield RefreshToken.remove({ userId: user.userId, clientId: client.clientId });
            yield AccessToken.remove({ userId: user.userId, clientId: client.clientId });

            var tokenValue = crypto.randomBytes(32).toString('base64');
            var refreshTokenValue = crypto.randomBytes(32).toString('base64');
            var accessToken = new AccessToken({ token: tokenValue, clientId: client.clientId, userId: user.userId });
            var refreshToken = new RefreshToken({ token: refreshTokenValue, clientId: client.clientId, userId: user.userId });

            yield refreshToken.save();
            yield accessToken.save();

            return done(null, tokenValue, refreshTokenValue, { 'expires_in': config.get('security:tokenLife') });
        }
        catch(err) {
            return done(err)
        }
    });


    //RefreshToken.findOne({ token: refreshToken }, function(err, token) {
    //    if (err) { return done(err); }
    //    if (!token) { return done(null, false); }
    //    if (!token) { return done(null, false); }
    //
    //    User.findById(token.userId, function(err, user) {
    //        if (err) { return done(err); }
    //        if (!user) { return done(null, false); }
    //
    //        RefreshToken.remove({ userId: user.userId, clientId: client.clientId }, function (err) {
    //            if (err) return done(err);
    //        });
    //        AccessToken.remove({ userId: user.userId, clientId: client.clientId }, function (err) {
    //            if (err) return done(err);
    //        });
    //
    //        var tokenValue = crypto.randomBytes(32).toString('base64');
    //        var refreshTokenValue = crypto.randomBytes(32).toString('base64');
    //        var token = new AccessToken({ token: tokenValue, clientId: client.clientId, userId: user.userId });
    //        var refreshToken = new RefreshToken({ token: refreshTokenValue, clientId: client.clientId, userId: user.userId });
    //
    //        refreshToken.save(function (err) {
    //            if (err) { return done(err); }
    //        });
    //
    //        token.save(function (err, token) {
    //            if (err) { return done(err); }
    //            done(null, tokenValue, refreshTokenValue, { 'expires_in': config.get('security:tokenLife') });
    //        });
    //    });
    //});
}));

/**
 * Saves a token with the given information
 * @param client
 * @param user
 * @param done
 */
function saveToken(client, user, done){
    var token = crypto.randomBytes(32).toString('base64'),
        accessToken = new AccessTokenModel({
            userId: user.id,
            clientId: client.id,
            token: token
        });

    accessToken.save(function(err) {
        if (err) { return done(err); }
        done(null, token);
    });
}

export const token = [
    passport.authenticate(['basic', 'oauth2-client-password'], { session: false }),
    oauth2Server.token(),
    oauth2Server.errorHandler()
];
