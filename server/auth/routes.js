import passport from "koa-passport";
import authAPI from "./api";
import { resolve } from "../core/ioc";
import { token } from "./oauth2";

export default {
    "/users": {
        method: "get",
        fn: authAPI.users
    },

    "/login": {
        method: "post",
        fn: () => {}
    },

    "/signup": {
        method: "post",
        fn: passport.authenticate("local-signup"),
        middleware: [resolve("requestBodyParser")]
    },

    "/profile": {
        method: "get",
        fn: () => {}
    },

    "/auth/token": {
        method: "post",
        fn: authAPI.getToken,
        middleware: [resolve("credentialParser")]
    },

    "/auth/refresh-token": {
        method: "post",
        fn: authAPI.refreshToken,
        middleware: [resolve("requestBodyParser")]
    }
};

