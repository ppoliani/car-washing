import co from "co";
import crypto from "crypto";
import jwt from "jwt-simple";
import User from "./models/User";
import AccessToken from "./models/AccessToken";
import RefreshToken from "./models/RefreshToken";

function *users() {
    this.body = yield new Promise((resolve) => {
        setTimeout(() => resolve(["User 1", "User 2"]), 3000);
    });
}

function *getToken() {
    const JWT_SECRET = process.env.SECURITY_JWT_SECRET;
    const TOKEN_LIFE = process.env.SECURITY_TOKEN_LIFE;

    try {
        let user;

        try {
            user = yield User.passwordMatches(this.username, this.password);
        }
        catch(ex) { // turn the exception to a response
            this.throw(403, ex.message)
        }

        yield RefreshToken.remove({ userId: user.id });
        yield AccessToken.remove({ userId: user.id });

        const tokenValue =  {
            expires_in: TOKEN_LIFE,
            userName: user.username,
            claims: user.claims
        };

        const jwtToken = jwt.encode(tokenValue, JWT_SECRET);
        const refreshTokenValue = crypto.randomBytes(32).toString('base64');
        const token = new AccessToken({
            token: jwtToken,
            userId: user.id,
            created: new Date()
        });
        const refreshToken = new RefreshToken({
            token: refreshTokenValue,
            userId: user.id
        });

        yield refreshToken.save();
        yield token.save();

        this.body = {
            jwtToken,
            refreshTokenValue,
            user_info: {
                userName: user.local.username,
                id: user.id,
                claims: user.claims
            }
        }
    }
    catch(ex) {
        throw ex;
    }
}

function *refreshToken() {
    const TOKEN_LIFE = process.env.SECURITY_TOKEN_LIFE;

    try {
        const token = yield RefreshToken.findOne({ token: this.req.body.refreshToken });

        if (!token) {
            this.throw(403, "Invalid refresh token")
        }

        const user = yield User.findById(token.userId);

        if(!user) {
            this.throw(403, "User not found")
        }

        yield RefreshToken.remove({ userId: user.id });
        yield AccessToken.remove({ userId: user.id });

        var tokenValue = crypto.randomBytes(32).toString('base64');
        var refreshTokenValue = crypto.randomBytes(32).toString('base64');
        var accessToken = new AccessToken({ token: tokenValue, userId: user.id });
        var refreshToken = new RefreshToken({ token: refreshTokenValue, userId: user.id });

        yield refreshToken.save();
        yield accessToken.save();

        this.body = {
            tokenValue,
            refreshTokenValue,
            expires_in: TOKEN_LIFE
        };
    }
    catch(ex) {
        throw ex;
    }
}

export default {
    users,
    getToken,
    refreshToken
};
