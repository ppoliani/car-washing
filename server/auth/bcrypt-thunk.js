import bcrypt from "bcrypt";

function encryptPassword(password) {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(10, function(err, salt) {
            bcrypt.hash(password, salt, function(err, hash) {
                if (err) { return reject(err); }
                return resolve(hash);
            });
        });
    });
}

function compare(data, hash) {
    return new Promise(function(resolve, reject) {
        bcrypt.compare(data, hash, function(err, matched) {
            if (err) { return reject(err); }
            return resolve(matched);
        });
    });
}

export default {
    encryptPassword,
    compare
};
