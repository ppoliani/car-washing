import mongoose from "mongoose";

const AccessToken = new mongoose.Schema({
    userId: {
        type: String,
        required: false
    },
    token: {
        type: String,
        unique: true,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

export default mongoose.model("AccessToken", AccessToken);
