import mongoose from "mongoose";

const Claim = new mongoose.Schema({
    type: String,
    value: String
});

export default mongoose.model("Claim", Claim);
