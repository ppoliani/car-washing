import mongoose from "mongoose";
import co from "co";
import bcrypt from "../bcrypt-thunk";
import Claim from "./Claim";

const User = mongoose.Schema({
    local: {
        username: String,
        password: String,
        email: String
    },

    claims: [Claim]
});

User.set('toJSON', {
    transform: function(doc, ret){
        delete ret.password;
    }
});

/**
 * Middlewares
 */
User.pre("save", function(done) {
    // only hash the password if it has been modified (or is new)
    if (!this.isModified("local")) {
        return done();
    }

    co.wrap(function *() {
        try {
            const hash = yield bcrypt.encryptPassword(this.local.password);
            this.local.password = hash;
            done();
        } catch (err) {
            done(err);
        }
    }).call(this).then(done);
});

User.statics.passwordMatches = function *(username, password) {
    var user = yield this.findOne({ "local.username": username.toLowerCase() }).exec();

    if (!user) {
        throw new Error("User not found");
    }

    if (yield bcrypt.compare(password, user.local.password)) {
        return user;
    }

    throw new Error("Password does not match");
};

export default mongoose.model("User", User);
