import passport from "koa-passport";
import passportLocal from "passport-local";
import co from "co";
import bcrypt from "./bcrypt-thunk";
import passportBearer from "passport-http-bearer";
import User from "./models/User";
import routes from "./routes";
import routeCreator from "../utils/routeCreator";
import AccessToken from "./models/AccessToken";
import { resolve } from "../core/ioc";

const logger = resolve("logger");
const BearerStrategy = passportBearer.Strategy;

// used to serialize the user for the session
passport.serializeUser(function(user, done) {
    done(null, user.id);
});

// used to deserialize the user
passport.deserializeUser(function(id, done) {
    User.findById(id, done);
});

passport.use(new BearerStrategy((accessToken, done) => {
    co(function* () {
        try {
            const token = yield AccessToken.findOne({token: accessToken});

            if(Math.round((Date.now() - token.created)/1000) > config.get('security:tokenLife')) {
                yield AccessToken.remove({token: accessToken});
                return done(null, false, { message: 'Token expired' });
            }

            const user = User.findById(token.userId);

            if (!user) {
                return done(null, false, { message: 'Unknown user' });
            }

            const info = { scope: '*', claims: user.claims };

            return done(null, user, info);
        }
        catch(err) {
            return done(null, false);
        }
    });
}));

passport.use("local-signup", new passportLocal.Strategy((username, password, done) => {
    co(function *() {
        try {
            const user = yield User.findOne({ "local.username": username });

            if (user) {
                return done(null, false);
            }
            else {
                const newUser = new User();

                newUser.local.username = username;
                newUser.local.password = password;
                yield newUser.save();

                logger.info("New user created");
            }
        }
        catch(ex) {
            logger.error(ex);
            done(ex);
        }
    }).then((user) => {
        done(null, user);
    });
}));

passport.use("local-login", new passportLocal.Strategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField : 'email',
    passwordField : 'password',
    passReqToCallback : true // allows us to pass back the entire request to the callback
}, (username, password, done) => {
    co(function *() {
        try {
            yield User.passwordMatches(username, password)
        }
        catch(ex) {
            return null;
        }
    }).then((user) => {
        done(null, user);
    });
}));

export default {
    routes: (router) =>  {
        routeCreator(router, routes)
    }
}
