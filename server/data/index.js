import mongoose from "mongoose";
import log from "../utils/logger";
import User from "../auth/models/User";
import AccessToken from "../auth/models/AccessToken";
import Claim from '../auth/models/Claim';
import RefreshToken from "../auth/models/RefreshToken";

let connectionString = undefined;
let connection = undefined;

export default function init () {
    connectionString = `mongodb://${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_NAME}`;
    mongoose.connect(connectionString);
    connection = mongoose.connection;

    connectionToDB();
    registertModels();
}

function connectionToDB() {
    connection.on('error', function (err) {
        log.error('connection error:', err.message);
    });

    connection.once('open', function callback () {
        log.info(`Connected to the ${connectionString}`);
    });
}

function registertModels() {
    connection.model("User", User);
    connection.model("AccessToken", AccessToken);
    connection.model("Claim", AccessToken);
    connection.model("RefreshToken", AccessToken);
}
