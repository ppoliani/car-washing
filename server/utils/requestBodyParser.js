import parse from "co-body";

export default function *(next) {
    this.req.body = yield parse(this);
    yield next;
}
