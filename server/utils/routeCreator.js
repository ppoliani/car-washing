import entries from "./entries";

export default function(router, routes) {
    for(let [endpoint, routeConfig] of entries(routes)) {
        router[routeConfig.method](endpoint, ...(routeConfig.middleware || []), routeConfig.fn)
    }
}
