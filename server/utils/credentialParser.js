import parse from "co-body";

export default function *(next) {
    const body = yield parse(this);

    this.username = body.username;
    this.password = body.password;
    this.scope = body.scope.split(";");

    yield next;
}
