import koa from "koa";
import ctk from "koa-connect";
import forceSSL from "koa-force-ssl";
import morgan from "morgan";
import session from "koa-generic-session";
import bodyParser from "koa-bodyparser";
import passport from "koa-passport";
import views from "koa-render";
import Router from "koa-router";
import flash from "connect-flash";
import appRouter from "./core/routes";
import cors from "./core/cors";
import errorHandling from "./core/errorHandling"
import dbConnection from "./data";

export default function bootstrap() {
    const app = koa();

    // trust proxy
    app.proxy = true;
    app.keys = [process.env.SESSION_KEY];

    // enable CORS
    cors(app);
    errorHandling(app);

    const router = appRouter(Router());

    app
        //.use(forceSSL())
        .use(router.routes())
        .use(router.allowedMethods())
        .use(ctk(morgan("dev")))
        .use(bodyParser())
        .use(session())
        .use(passport.initialize())
        .use(passport.session())
        .use(ctk(flash))
        .use(views('./views', {
            map: { html: 'handlebars' },
            cache: false
        }));

    dbConnection();

    return app;
}
