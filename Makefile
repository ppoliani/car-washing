.PHONY: build clean env certificates

env:
	cp -R dotenv.sample .env

certificates:
	mkdir -p "dist/certificates" && cp -R certificates/ dist/certificates

clean:
	rm -fr dist/

build: clean env certificates
	./node_modules/.bin/babel --watch server/ --out-dir dist/ --no-comments
